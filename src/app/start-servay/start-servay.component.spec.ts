import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartServayComponent } from './start-servay.component';

describe('StartServayComponent', () => {
  let component: StartServayComponent;
  let fixture: ComponentFixture<StartServayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartServayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartServayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
